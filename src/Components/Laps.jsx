import React from "react";
import { useSelector } from "react-redux";
import "./css/laps.css";

const Laps = () => {
  let { laps } = useSelector((state) => state);

  return (
    <div className="laps">
      {laps.map((lap) => (
        <div className="lapInfo">
          <h4>{lap.num < 10 ? "0" + lap.num : lap.num}</h4>
          <p>
            <span>{lap.hr < 10 ? "0" + lap.hr : lap.hr} : </span>
            <span>{lap.min < 10 ? "0" + lap.min : lap.min} : </span>
            <span>{lap.sec < 10 ? "0" + lap.sec : lap.sec} : </span>
            <span>{lap.milliSec < 10 ? "0" + lap.milliSec : lap.milliSec}</span>
          </p>
        </div>
      ))}
    </div>
  );
};

export default Laps;
