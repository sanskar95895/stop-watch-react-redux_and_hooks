import './App.css';
import LapButton from './Components/LapButton';
import StartButton from './Components/StartButton';
import StopButton from './Components/StopButton';
import ResetButton from './Components/ResetButton';
import StopwatchTimer from './Components/StopwatchTimer';
import Laps from './Components/Laps';

function App() {
  return (
    <div className="App">
      <StopwatchTimer />
      <StartButton />
      <StopButton />
      <ResetButton />
      <LapButton />
      <Laps />
    </div>
  );
}

export default App;
